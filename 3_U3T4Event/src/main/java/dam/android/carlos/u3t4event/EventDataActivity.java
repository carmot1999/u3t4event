package dam.android.carlos.u3t4event;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private String[] months;

    private String priority = "Normal";

    private TextView tvEventName;
    private EditText etPlace;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        months = getResources().getStringArray(R.array.months);

        setUI();

        // get data from Intent that started this activity
        Bundle inputData = getIntent().getExtras();

        // set eventName from inputData
        tvEventName.setText(inputData.getString("EventName"));

        if (inputData.getString("Priority") != null) {
            etPlace.setText(inputData.getString("Place"));

            int monthNumber = 0;

            for (int i = 0; i < months.length && monthNumber == 0; i++) {
                if (months[i].equals(inputData.getString("Month"))) {
                    monthNumber = i;
                }
            }

            dpDate.updateDate(Integer.parseInt(inputData.getString("Year")),  monthNumber, Integer.parseInt(inputData.getString("Day")));
            tpTime.setHour(Integer.parseInt(inputData.getString("Hour").split(":")[0]));
            tpTime.setMinute(Integer.parseInt(inputData.getString("Hour").split(":")[1]));
        }

    }

    private void setUI() {
        tvEventName = (TextView) findViewById(R.id.tvEventName);
        etPlace = (EditText) findViewById(R.id.etPlace);
        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);
        dpDate = (DatePicker) findViewById(R.id.dpDate);

        //TODO: Mostramos u ocultamos el calendario de DatePicker
        if (this.getApplicationContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            //TODO: Si el móvil está en vertical...
            dpDate.setCalendarViewShown(false);
        } else {
            //TODO: Si el móvil está en horizontal...
            dpDate.setCalendarViewShown(true);
        }

        tpTime = (TimePicker) findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);

        rgPriority.check(R.id.rbNormal);

        // set listeners
        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch (view.getId()) {
            case R.id.btAccept:
                eventData.putString("EventData", "PLACE: " + etPlace.getText().toString() + "\n" +
                        "PRIORITY: " + priority + "\n" +
                        "DATE: " + (dpDate.getDayOfMonth() + " " + months[dpDate.getMonth()] + " " + dpDate.getYear()) + "\n" +
                        "HOUR: " + (tpTime.getHour() + ":" + tpTime.getMinute()));
                break;
            case R.id.btCancel:
                // get data from Intent that started this activity
                Bundle inputData = getIntent().getExtras();

                if (inputData.getString("Priority") != null) {
                    eventData.putString("EventData", "PLACE: " + inputData.getString("Place") + "\n" +
                            "PRIORITY: " + inputData.getString("Priority") + "\n" +
                            "DATE: " + (inputData.getString("Day") + " " + inputData.getString("Month") + " " + inputData.getString("Year")) + "\n" +
                            "HOUR: " + inputData.getString("Hour"));
                }

                break;
        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int i) {
        switch (i) {
            case R.id.rbLow:
                priority = "Low";
                break;
            case R.id.rbNormal:
                priority = "Normal";
            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }
}
