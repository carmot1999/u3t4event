package dam.android.carlos.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    // TODO: Array de meses.
    private String[] months;

    private String priority = "Normal";

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        // TODO: Cargamos el array de meses.
        months = getResources().getStringArray(R.array.months);

        setUI();

        // get data from Intent that started this activity
        Bundle inputData = getIntent().getExtras();

        // set eventName from inputData
        tvEventName.setText(inputData.getString("EventName"));
    }

    private void setUI() {
        tvEventName = (TextView) findViewById(R.id.tvEventName);
        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);
        dpDate = (DatePicker) findViewById(R.id.dpDate);
        tpTime = (TimePicker) findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);

        rgPriority.check(R.id.rbNormal);

        // set listeners
        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch (view.getId()) {
            case R.id.btAccept:

                // TODO: Recogemos el mes desde el array de meses global.
                eventData.putString("EventData", "Priority: " + priority + "\n" +
                        "Month: " + months[dpDate.getMonth()] + "\n" +
                        "Day: " + dpDate.getDayOfMonth() + "\n" +
                        "Year: " + dpDate.getYear());
                break;
            case R.id.btCancel:

                // get data from Intent that started this activity
                Bundle inputData = getIntent().getExtras();

                if (inputData.getString("Priority") != null) {
                    eventData.putString("EventData", "Priority: " + inputData.getString("Priority") + "\n" +
                            "Month: " + inputData.getString("Month") + "\n" +
                            "Day: " + inputData.getString("Day") + "\n" +
                            "Year: " + inputData.getString("Year"));
                }

                break;
        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int i) {
        switch (i) {
            case R.id.rbLow:
                priority = "Low";
                break;
            case R.id.rbNormal:
                priority = "Normal";
            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }
}
