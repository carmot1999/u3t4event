package dam.android.carlos.u3t4event;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class U3T4EventActivity extends LogActivity {

    private final int REQUEST = 0;

    private EditText etEventName;
    private TextView tvCurrentData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u3_t4_event);

        setUI();

        // TODO: Recuperamos Current Data.
        if (savedInstanceState != null) {
            TextView tvCurrentData = findViewById(R.id.tvCurrentData);
            tvCurrentData.setText(savedInstanceState.getString("CurrentData"));
        }
    }

    private void setUI() {
        etEventName = (EditText) findViewById(R.id.etEventName);
        tvCurrentData = (TextView) findViewById(R.id.tvCurrentData);
        // clean text view
        tvCurrentData.setText("");
    }

    public void editEventData(View v) {
        Intent intent = new Intent(this, EventDataActivity.class);
        Bundle bundle = new Bundle();

        //set info data to bundle
        bundle.putString("EventName", etEventName.getText().toString());

        // TODO: Enviamos datos del Event a editar.
        if (!tvCurrentData.getText().toString().equals("")) {
            String[] data = tvCurrentData.getText().toString().split("\\s+");

            bundle.putString("Place", data[1]);
            bundle.putString("Priority", data[3]);
            bundle.putString("Day", data[5]);
            bundle.putString("Month", data[6]);
            bundle.putString("Year", data[7]);
            bundle.putString("Hour", data[9]);
        }

        // add bundle to intent
        intent.putExtras(bundle);

        startActivityForResult(intent, REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST && resultCode == RESULT_OK) {
            tvCurrentData.setText(data.getStringExtra("EventData"));
        }

    }
}
