package dam.android.carlos.u3t4event;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import java.text.DateFormat;
import java.util.Calendar;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener {

    // TODO: Variables globales para TimePicker, ya que van a ser llamadas de forma externa.
    TextView chooseTime;
    String amPm;

    private String[] months;

    private String priority = "Normal";

    private TextView tvDatePicked;
    private TextView tvEventName;
    private EditText etPlace;
    private RadioGroup rgPriority;
    private Button btAccept;
    private Button btCancel;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        months = getResources().getStringArray(R.array.months);

        setUI();

        // get data from Intent that started this activity
        Bundle inputData = getIntent().getExtras();

        // set eventName from inputData
        tvEventName.setText(inputData.getString("EventName"));

        // TODO: Mostramos DatePicker.
        Button btDate = (Button) findViewById(R.id.btDate);
        btDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });

        // TODO: Mostramos TimePicker.
        Button btTime = (Button) findViewById(R.id.btTime);
        btTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog;
                Calendar calendar;
                int currentHour;
                int currentMinute;

                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMinute = calendar.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(EventDataActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                        if (hourOfDay >= 12) {
                            amPm = " PM";
                        } else {
                            amPm = " AM";
                        }
                        chooseTime.setText(String.format("%02d:%02d", hourOfDay, minutes) + amPm);
                    }
                }, currentHour, currentMinute, false);

                timePickerDialog.show();
            }
        });

        if (inputData.getString("Priority") != null) {
            etPlace.setText(inputData.getString("Place"));
            tvDatePicked.setText(inputData.getString("Month") + " " + inputData.getString("Day") + " " + inputData.getString("Year"));
            chooseTime.setText(inputData.getString("Hour"));
        }

    }

    private void setUI() {
        tvEventName = (TextView) findViewById(R.id.tvEventName);
        etPlace = (EditText) findViewById(R.id.etPlace);
        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);

        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);

        tvDatePicked = (TextView) findViewById(R.id.tvDatePicked);
        chooseTime = (TextView) findViewById(R.id.tvTime);

        rgPriority.check(R.id.rbNormal);

        // set listeners
        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch (view.getId()) {
            case R.id.btAccept:
                eventData.putString("EventData", "PLACE: " + etPlace.getText().toString() + "\n" +
                        "PRIORITY: " + priority + "\n" +
                        "DATE: " + tvDatePicked.getText().toString() + "\n" +
                        "HOUR: " + chooseTime.getText().toString());
                break;
            case R.id.btCancel:
                // get data from Intent that started this activity
                Bundle inputData = getIntent().getExtras();

                if (inputData.getString("Priority") != null) {
                    eventData.putString("EventData", "PLACE: " + inputData.getString("Place") + "\n" +
                            "PRIORITY: " + inputData.getString("Priority") + "\n" +
                            "DATE: " + inputData.getString("Month") + " " + (inputData.getString("Day") + " " + inputData.getString("Year")) + "\n" +
                            "HOUR: " + inputData.getString("Hour"));
                }

                break;
        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int i) {
        switch (i) {
            case R.id.rbLow:
                priority = "Low";
                break;
            case R.id.rbNormal:
                priority = "Normal";
            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());

        // TODO: Mostramos fecha seleccionada.
        TextView tvDatePicked = (TextView) findViewById(R.id.tvDatePicked);
        tvDatePicked.setText(currentDateString);
    }

}
