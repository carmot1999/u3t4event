package dam.android.carlos.u3t4event;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class LogActivity extends AppCompatActivity {

    private final String DEBUG_TAG = "LOG-" + this.getClass().getSimpleName();

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(DEBUG_TAG, "onSaveInstanceState");

        // TODO: Guardamos Current Data.
        TextView tvCurrentData = findViewById(R.id.tvCurrentData);
        outState.putString("CurrentData", tvCurrentData.getText().toString());

        super.onSaveInstanceState(outState);
    }

}

